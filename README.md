# Lab : Transmission des données de configuration à un conteneur sous Kubernetes


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

Kubernetes propose plusieurs options pour stocker et gérer les données de configuration. Ce lab se concentrera sur le processus de transmission de ces données de configuration à vos conteneurs afin de configurer les applications. Nous aurons  l'opportunité de travailler concrètement avec la configuration des applications dans Kubernetes en transmettant certaines données de configuration existantes stockées dans Secrets et ConfigMaps vers un conteneur.

Nous allons :

1. Générez un fichier `htpasswd` et stockez-le comme secret

2. Créer un pod Nginx

# Contexte

Vous travaillez pour BeeHive, une entreprise qui assure des expéditions régulières d'abeilles aux clients. L'entreprise travaille au déploiement de certaines applications sur un cluster Kubernetes.

L'une de ces applications est un simple serveur Web **Nginx**. Ce serveur est utilisé dans le cadre d'une application backend sécurisée et l'entreprise souhaite qu'il soit configuré pour utiliser l'authentification de base HTTP.

Cela nécessitera un ``htpasswd`` fichier ainsi qu'un fichier de configuration Nginx personnalisé. Afin de déployer ce serveur Nginx sur le cluster avec de bonnes pratiques de configuration, vous devrez charger la configuration **Nginx** personnalisée à partir d'un ConfigMap et utiliser un secret pour stocker les données ``htpasswd``.

Créez un pod avec un conteneur exécutant l' ``nginx:1.19.1image``. Fournissez une configuration Nginx personnalisée à l'aide d'un ConfigMap et remplissez un htpasswdfichier à l'aide d'un secret.

htpasswdest déjà installé sur le serveur, et vous pouvez générer un ``htpasswd`` fichier comme ceci :

``htpasswd -c .htpasswd user``
Un pod appelé busyboxexiste déjà dans le cluster, que vous pouvez utiliser pour contacter votre pod Nginx et tester votre configuration.


>![Alt text](img/image.png)

# Pratique 

# 1. Connexion au control node

```bash
ssh -i id_rsa user@public_ip_address
```

# 2. Génération du fichier htpasswd

Générez un fichier htpasswd  et stockez-le comme secret

```bash
htpasswd -c .htpasswd user
```

>![Alt text](img/image-1.png)
*Création du htpdasswd réussi*

Créez un mot de passe dont vous pourrez facilement vous souvenir (nous en aurons besoin plus tard).

# 3. Afficher le contenu du fichier :

```bash
cat .htpasswd
```
>![Alt text](img/image-2.png)

# 4. Créez un secret contenant les htpasswd :

```bash
kubectl create secret generic nginx-htpasswd --from-file .htpasswd
```

>![Alt text](img/image-3.png)
*Création du secret*

# 5. Supprimez le .htpasswdfichier :

```bash
rm .htpasswd
```
Nous pouvons supprimer ce fichier pour des "raisons de sécurité"

# 6. Créer le pod Nginx

```bash
nano pod.yml
```

Entrez ce qui suit pour créer le pod et monter la configuration Nginx et htpasswdles données secrètes :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.19.1
    ports:
    - containerPort: 80
    volumeMounts:
    - name: config-volume
      mountPath: /etc/nginx
    - name: htpasswd-volume
      mountPath: /etc/nginx/conf
  volumes:
  - name: config-volume
    configMap:
      name: nginx-config
  - name: htpasswd-volume
    secret:
      secretName: nginx-htpasswd
```

Enregistrez et quittez le fichier

# 6. Affichez le ConfigMap existant :

```bash
kubectl get cm
```

>![Alt text](img/image-4.png)
*Liste de ConfigMap*

# 6. Obtenir plus d'informations sur nginx-config :

```bash
kubectl describe cm nginx-config
```

>![Alt text](img/image-5.png)
*Contenu du ConfigMap*

Création du pod :

```bash
kubectl apply -f pod.yml
```

# 7. Vérifiez l'état du pod et obtentions son adresse IP :

```bash
kubectl get pods -o wide
```



Son adresse IP sera répertoriée une fois qu'il aura un Runningstatut. Nous l'utiliserons dans les deux dernières commandes.

Dans le busyboxpod existant, sans utiliser d'informations d'identification, vérifiez que tout fonctionne :

```bash
kubectl exec busybox -- curl <NGINX_POD_IP>
```

Nous verrons du HTML pour la 401 Authorization Required page – mais c'est une bonne chose, car cela signifie que notre configuration fonctionne comme prévu.

Exécutez à nouveau la même commande à l'aide des informations d'identification (y compris le mot de passe que vous avez créé au début de l'atelier) pour vérifier que tout fonctionne :


```bash
kubectl exec busybox -- curl -u user:<PASSWORD> <NGINX_POD_IP>
```

Cette fois, nous verrons la version : Welcome to nginx!.
